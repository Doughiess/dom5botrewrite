from discord.ext import commands
import GameOptions as go
import os
import RoleAndChannel as rac
import asyncio
import subprocess
import shlex
import logging
import Database as db
import psycopg2

# TODO recheck everything to make it work with db

description = '''Dominions 5 hosting bot'''
bot = commands.Bot(command_prefix='%', description=description)

logging.basicConfig(filename='dom5hostbot.log', level=logging.INFO,
                    format='%(asctime)s:%(levelname)s:%(name)s: %(message)s')
bot_token = input()
dblogin = input()
dbpw = input()


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')
    go.gameCount = len(db.get_all_games(dblogin, dbpw))
    print('{} games are currently running'.format(go.gameCount))
    print('{} games are currently being tracked'.format(len(db.get_tracked_port(dblogin, dbpw))))
    bot.remove_command('repeat')


@bot.command()
async def host(ctx, help='Begins the dominions 5 hosting process, bot should query you as to settings'):
    """Hosting begins here, hands off to GameOptions.request_game_options"""
    logging.info("{}, code {} began hosting process.".format(ctx.author, ctx.author.mention))
    prospective_game = await go.request_game_options(ctx)
    if prospective_game is None:
        await ctx.author.send('Game creation cancelled due to timeout.')
        logging.warning("{}, code {} hosting process timed out.".format(ctx.author, ctx.author.mention))
    else:
        try:
            chosen_port = db.insert_game(dblogin, dbpw, prospective_game)
            mkdir_string = 'mkdir -p {}/{}'.format(go.savesPath, prospective_game.game_name)
            copy_string = 'cp {} {}/{}/turnBackupScript.sh'.format(go.backupScript, go.savesPath,
                                                                  prospective_game.game_name)
            exit_code = subprocess.call(shlex.split(mkdir_string))
            if exit_code != 0:
                await ctx.author.send('Failed to make directory for script, game saves may not be backed up, '
                                      'please report this error.')
                logging.critical(
                    "{}, code {} failed to create directory for script".format(ctx.author, ctx.author.mention))
            else:
                exit_code = subprocess.call(shlex.split(copy_string))
                if exit_code != 0:
                    await ctx.author.send('Failed to copy backup script, game saves may not be backed up,'
                                          ' please report this error.')
                    logging.critical("{}, code {} failed to copy backup script.".format(ctx.author, ctx.author.mention))
                hosted_game_string = await go.hosted_game_string(prospective_game, chosen_port)
                await ctx.author.send(hosted_game_string)
                go.gameCount += 1
                new_host_role = await rac.create_role_after_host(ctx, '{}_host'.format(prospective_game.game_name))
                new_channel = await rac.create_channel_after_host(ctx, prospective_game.game_name)
                await rac.give_host_role(ctx, new_host_role)
                await rac.create_game_role(ctx, prospective_game.game_name)
                # slice out masterpass because I'm lazy as fuck
                hosted_string_no_pass = hosted_game_string[:hosted_game_string.find('Master pass:')]
                hosted_string_remainder = hosted_game_string[hosted_game_string.find('Hosting interval:'):]
                sent_game_string = await new_channel.send(hosted_string_no_pass + hosted_string_remainder)
                await sent_game_string.pin()
                await new_channel.send('{} your new created games channel is here. The game session is not currently '
                                       'running, to host it, type {}startsession. The game will then be on pretender '
                                       'submission.'.format(ctx.author.mention, bot.command_prefix))
                logging.info("{}, code {} game creation succeeded".format(ctx.author, ctx.author.mention))
        except:
            await ctx.message.channel.send(f'There was an error inserting the game {prospective_game.game_name}into the'
                                           f'database, please report this.')
            logging.error(f'{ctx.author} code {ctx.author.mention} tried hosting a game which failed to insert, '
                          f'prospective game was {prospective_game}')
            raise


@bot.command()
async def listcommands(ctx, help='Lists all available commands.'):
    commands_string_list = []
    for commands in bot.commands:
        commands_string_list.append(commands.name)
    commands_string = ', '.join(commands_string_list)
    commands_string += '\nTry {}help (command) for further help.'.format(bot.command_prefix)
    await ctx.channel.send(commands_string)


@bot.command()
async def startsession(ctx, help='Starts a game session (puts it on pretender submission) that the database knows about'
                                 'from the host command. Must be used in game channel.'):
    # start the screen session
    row = db.get_game_details(dblogin, dbpw, ctx.message.channel.name)[0]
    opts_from_row = go.GameOptions()
    opts_from_row.init_from_query(row)
    await ctx.channel.send('Trying to start game session.')
    host_exit_code = await go.attempt_host(opts_from_row, row[18])
    if host_exit_code == -1:
        await ctx.channel.send('Starting the game session failed for some reason, report this.')
        logging.error(f'{ctx.author}, code {ctx.author.mention} tried to start a game session but failed.')
    else:
        await ctx.channel.send('Game session started successfully.')


@bot.command()
async def listmaps(ctx, help='Lists maps available on server.'):
    """List the available maps and send message to channel"""
    map_string_list = await get_maps_list()
    for map_str in map_string_list:
        await ctx.channel.send(map_str)


async def get_maps_list():
    map_dir = go.mapsPath + '/'
    map_string_list = []
    map_string = '```'
    for f in os.listdir(map_dir):
        if os.path.isfile(os.path.join(map_dir, f)) and f.endswith('.map'):
            if len(map_string) + len(f) + 4 >= 2048:
                map_string += '```'
                map_string_list.append(map_string)
                map_string = '```{}\n'.format(f)
            else:
                map_string += f
                map_string += '\n'
    map_string += '```'
    map_string_list.append(map_string)
    return map_string_list


@bot.command()
async def listmods(ctx, help='Lists the available mods on server.'):
    """List the available mods and send message to channel"""
    mod_string_list = await get_mods_list()
    for mod_str in mod_string_list:
        await ctx.channel.send(mod_str)


async def get_mods_list():
    mods_dir = go.modsPath + '/'
    mod_string_list = []
    mods_string = '```'
    for f in os.listdir(mods_dir):
        if os.path.isfile(os.path.join(mods_dir, f)) and f.endswith('.dm'):
            if len(mods_string) + len(f) + 4 >= 2048:
                mods_string += '```'
                mod_string_list.append(mods_string)
                mods_string = '```{}\n'.format(f)
            else:
                mods_string += f
                mods_string += '\n'
    mods_string += '```'
    mod_string_list.append(mods_string)
    return mod_string_list


async def role_check(ctx):
    split_message = ctx.message.content.split(' ')
    if len(split_message) != 1:
        host_role = await get_role_by_name(ctx.guild, '{}_host'.format(split_message[1]))
    else:
        host_role = await get_role_by_name(ctx.guild, '{}_host'.format(ctx.channel))
    gm_role = await get_role_by_name(ctx.guild, 'Gamemasters')
    caller_has_role = await check_member_role(ctx.author, host_role)
    if caller_has_role or ctx.author.top_role >= gm_role:
        return True
    else:
        await ctx.channel.send('You do not have the required role to change the timer for this game')
        return False


@bot.command()
@commands.check(role_check)
async def deletegame(ctx, help='Deletes a running dom5 game, its related channel, deletes the roles and deleting game'
                               ' files, requires either Gamemaster or higher rank or to be the host of the game, must '
                               'be used in the hosted games channel.'):
    """Delete a running dom5 game, closing the channel, deleting the roles, stopping the games
    screen session and deleting the game files"""
    await ctx.message.channel.send('Do you really want to delete the game, channel, roles and game saved files?\
 Enter YES to confirm, anything else to cancel.')
    logging.info("{}, code {} started game deletion for game {}"
                 .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
    try:
        msg = await ctx.bot.wait_for('message', timeout=120.0)
        if msg.content == 'YES':
            logging.info("{}, code {} accepted game deletion for game {}"
                         .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
            game_channel = ctx.message.channel
            game_role = await get_role_by_name(ctx.guild, ctx.message.channel.name)
            game_host_role = await get_role_by_name(ctx.guild, '{}_host'.format(ctx.message.channel.name))
            await game_role.delete(reason='Deleting per request by {}'.format(ctx.message.author.nick))
            await game_host_role.delete(reason='Deleting per request by {}'.format(ctx.message.author.nick))
            await game_channel.delete(reason='Deleting per request by {}'.format(ctx.message.author.nick))
            screen_command = 'screen -X -S {} quit'.format(game_channel.name)
            exit_code = subprocess.call(shlex.split(screen_command))
            if exit_code == 0:
                await ctx.message.author.send('Game session closed successfully.')
            else:
                await ctx.message.author.send('Failed to close game session, please report this error.')
                logging.critical("{}, code {} screen session failed to close {}"
                                 .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
            rm_command = 'rm -R {}{}'.format(go.savesPath, ctx.message.channel.name)
            exit_code = subprocess.call(shlex.split(rm_command))
            if exit_code == 0:
                await ctx.message.author.send('Successfully removed game files.')
                go.gameCount -= 1
            else:
                await ctx.message.author.send('Failed to remove game save files, please report this error.')
                logging.critical("{}, code {} failed to remove game files for {}"
                                 .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
            try:
                db.remove_game(dblogin, dbpw, ctx.message.channel.name)
            except:
                await ctx.message.author.send('Failed to remove game database entry, please report this error.')
                logging.critical("{}, code {} failed to remove game database entry for {}"
                                 .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
        else:
            await ctx.message.channel.send('Cancelled.')
            logging.info("{}, code {} cancelled game deletion for game {}"
                         .format(ctx.author, ctx.author.mention, ctx.message.channel.name))
    except asyncio.TimeoutError:
        await ctx.message.channel.send('Cancelled due to timeout')
        logging.info("{}, code {} cancelled game deletion with timeout for game {}"
                     .format(ctx.author, ctx.author.mention, ctx.message.channel.name))


@bot.check
async def require_rank(ctx):
    if ctx.guild is not None:
        returned_role = await get_role_by_name(ctx.guild, 'Autohost')
        if returned_role is not None:
            allowed = await check_member_role(ctx.author, returned_role)
            return allowed
    else:
        return True 


@bot.command()
@commands.check(role_check)
async def settimer(ctx, game_name: str, timer: str, help='Sets the remaining time on a turn for a given game. '
                                                         'Usage: {}settimer (game name) (time in dhm: e.g. 0d15h0m). '
                                                         'Requires either Gamemaster or higher role or game host role.'
                   .format(bot.command_prefix)):
    new_timer = await go.interpret_host_interval(timer)
    logging.info("{}, code {} changing timer for game: {}".format(ctx.author, ctx.author.mention, game_name))  
    if new_timer != -1:
        write_string = 'settimeleft {}'.format(new_timer * 60)
        with open('{}/{}/domcmd'.format(go.savesPath, game_name), 'a') as dom_cmd:
            dom_cmd.write(write_string)
            dom_cmd.close()
            await ctx.channel.send('Timer successfully adjusted, this takes a minute to show up in game.')
    else:
        await ctx.channel.send('Timer was not adjusted due to an error, likely due to the formatting of the time input.')
        
            
@bot.command()
@commands.check(role_check)
async def startgame(ctx, game_name: str, help='Starts an already created game. Usage: {}startgame (game name). Requires'
                                              ' either Gamemaster or higher role or game host role.'.
                    format(bot.command_prefix)):
    write_string = 'settimeleft 1'
    logging.info("{}, code {} trying to start game: {}".format(ctx.author, ctx.author.mention, game_name))
    with open('{}/{}/domcmd'.format(go.savesPath, game_name), 'a') as dom_cmd:
        dom_cmd.write(write_string)
        dom_cmd.close()
        await ctx.channel.send('Game started.')


@bot.command()
@commands.check(role_check)
async def sethostinterval(ctx, game_name: str, timer: str, help='Sets the hosting interval for each turn for a given '
                                                                'game, this will also increase the amount of remaining '
                                                                'time on the turn to this interval. '
                                                                'Usage: {}sethostinterval (game name) '
                                                                '(time in dhm: e.g. 0d15h0m). Requires either '
                                                                'Gamemaster or higher role or game host role.'.
                          format(bot.command_prefix)):
    logging.info("{}, code {} trying to set host interval for {}".format(ctx.author, ctx.author.mention, game_name))
    host_interval = await go.interpret_host_interval(timer)
    if host_interval != -1:
        write_string = 'setinterval {}'.format(host_interval)
        with open ('{}/{}/domcmd'.format(go.savesPath, game_name), 'a') as dom_cmd:
            dom_cmd.write(write_string)
            dom_cmd.close() 
            await ctx.channel.send('Timer successfully adjusted, this takes a minute to show up in game.')
        try:
            db.change_timer(dblogin, dbpw, game_name, host_interval)
        except:
            logging.error(f'{ctx.author}, code: {ctx.author.mention} changed the hosting interval for {game_name}'
                          f', but this was not reflected in the database.')
            await ctx.channel.send('Timer was adjusted, but this change was not reflected in the database, please report this'
                             'error.')
    else:
        await ctx.channel.send('Timer was not adjusted due to an error, likely due to the formatting of the time input.')      


async def single_attachment_check(ctx):
    if len(ctx.message.attachments) == 1:
        return True
    else:
        return False


@bot.command()
@commands.has_any_role('Gamemasters', 'Admin')
@commands.check(single_attachment_check)
async def uploadmap(ctx, help='Attempts to upload a .zip formatted map to the server, only possible by Gamemasters '
                              'or above roles. To use, click the plus sign to upload a file, upload the .zip file and'
                              ' put {}uploadmap as the comment.'.format(bot.command_prefix)):
    logging.info("{}, code {} trying to upload a map {}".format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
    maps_path = go.mapsPath + '/'
    map_str_list = await get_maps_list()
    await ctx.channel.send('Please check the map you are uploading is not already on the server, or that the name '
                           'is not already in use and that the file is in .zip format.')
    for map_str in map_str_list:
        await ctx.channel.send(map_str)
    await ctx.channel.send('Confirm upload with yes, any other input will cancel.')
    msg = await ctx.bot.wait_for('message', timeout=60.0)
    if msg.content == 'yes':
        logging.info("{}, code {} chose to upload map: {}".format(ctx.author, ctx.author.mention,
                                                                  ctx.message.attachments[0].filename))
        map_attachment = ctx.message.attachments[0]
        wget_string = 'wget --quiet -P {} {}'.format(maps_path, map_attachment.url)
        ret_code = subprocess.call(shlex.split(wget_string))
        if ret_code == 0:
            await ctx.channel.send('File uploaded successfully.')
            unzip_string = 'unzip -d {} -n -q {}{}'.format(maps_path, maps_path, map_attachment.filename)
            ret_code_unzip = subprocess.call(shlex.split(unzip_string))
            if ret_code_unzip == 0:
                await ctx.channel.send('Unzipping was successful.')
                chmod_string = 'chmod 775 -R {}'.format(maps_path)
                ret_code_chmod = subprocess.call(shlex.split(chmod_string))
                if ret_code_chmod == 0:
                    await ctx.channel.send('Map file may now be used.')
                else:
                    await ctx.channel.send('Error with file permissions, please report this error.'
                                           ' Map may not be used.')
                    logging.critical("{}, code {} error with file permissions with string: {}"
                                     .format(ctx.author, ctx.author.mention, chmod_string))
            else:
                await ctx.channel.send('Unzipping failed, you may have tried to accidentally overwrite something, '
                                       'if not, please report this error.')
                logging.critical("{}, code {} unzipping failed with string: {}"
                                 .format(ctx.author, ctx.author.mention, unzip_string))
            rm_string = 'rm {}{}'.format(maps_path, map_attachment.filename)
            ret_code_rm = subprocess.call(shlex.split(rm_string))
            if ret_code_rm != 0:
                await ctx.channel.send('Removing uploaded file failed, please report this error.')
                logging.error("{}, code {} failed to remove uploaded map: {}"
                              .format(ctx.author, ctx.author.mention, rm_string))
        else:
            await ctx.channel.send('File download failed, please report this error.')
            logging.critical("{}, code {} failed to download map {}".format(ctx.author, ctx.author.mention,
                                                                            ctx.message.attachments[0].filename))
    else:
        await ctx.channel.send('Map upload cancelled.')
        logging.info("{}, code {} cancelled map upload".format(ctx.author, ctx.author.mention))


@bot.command()
@commands.has_any_role('Gamemasters', 'Admin')
@commands.check(single_attachment_check)
async def uploadmod(ctx, help='Attempts to upload a .zip formatted mod to the server, only possible by Gamemasters or '
                              'above roles. To use, click the plus sign to upload a file, upload the .zip file and '
                              'put {}uploadmod as the comment.'.format(bot.command_prefix)):
    logging.info("{}, code {} trying to upload a mod {}".format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
    mod_str_list = await get_mods_list()
    await ctx.channel.send('Please check the mod you are uploading is not already on the server, or that the name '
                           'is not already in use and that the file is in .zip format.')
    for mod_str in mod_str_list:
        await ctx.channel.send(mod_str)
    await ctx.channel.send('Confirm upload with yes, any other input will cancel.')
    msg = await ctx.bot.wait_for('message', timeout=60.0)
    mods_path = go.modsPath + '/'
    if msg.content == 'yes':
        logging.info("{}, code {} chose to upload a mod {}".format(ctx.author, ctx.author.mention,
                                                                    ctx.message.attachments[0].filename))
        mod_attachment = ctx.message.attachments[0]
        wget_string = 'wget --quiet -P {} {}'.format(mods_path, mod_attachment.url)
        ret_code = subprocess.call(shlex.split(wget_string))
        if ret_code == 0:
            await ctx.channel.send('File uploaded successfully.')
            unzip_string = 'unzip -d {} -n -q {}{}'.format(mods_path, mods_path, mod_attachment.filename)
            ret_code_unzip = subprocess.call(shlex.split(unzip_string))
            if ret_code_unzip == 0:
                await ctx.channel.send('Unzipping was successful.')
                chmod_string = 'chmod 775 -R {}'.format(mods_path)
                ret_code_chmod = subprocess.call(shlex.split(chmod_string))
                if ret_code_chmod == 0:
                    await ctx.channel.send('Mod file may now be used.')
                    logging.info("{}, code {} successfully uploaded mod {}".
                                 format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
                else:
                    await ctx.channel.send('Error with file permissions, please report this error. '
                                           'Mod may not be used.')
                    logging.error("{}, code {} failed mod upload due to file permissions.".
                                  format(ctx.author, ctx.author.mention))
            else:
                await ctx.channel.send('Unzipping failed, you may have tried to accidentally overwrite something, '
                                       'if not, please report this error.')
                logging.error("{}, code {} failed mod upload {} due to zipping".
                              format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
            rm_string = 'rm {}{}'.format(mods_path, mod_attachment.filename)
            ret_code_rm = subprocess.call(shlex.split(rm_string))
            if ret_code_rm != 0:
                await ctx.channel.send('Removing uploaded file failed, please report this error.')
                logging.error("{}, code {} mod upload {} failed to remove file".
                              format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
        else:
            await ctx.channel.send('File download failed, please report this error.')
            logging.error("{}, code {} mod upload {} failed to download".
                          format(ctx.author, ctx.author.mention, ctx.message.attachments[0].filename))
    else:
        await ctx.channel.send('Mod upload cancelled.')
        logging.info("{}, code {} cancelled mod upload".format(ctx.author, ctx.author.mention))


async def get_role_by_name(guild, role_name):
    for role in guild.roles:
        if role_name == role.name:
            return role
    return None


async def check_member_role(member, check_role):
    for role in reversed(member.roles):
        if role == check_role:
            return True
    return False


@bot.command()
async def owner(ctx, help='Output owner'):
    await ctx.channel.send('Owned by DoorStuck, send any complaints, bugs or errors to him.')


@bot.command()
async def track(ctx, check=role_check, help='Track a game by checking every 30 minutes for the game status and '
                                            'sending reminders with less than an hour to go. Must be used in game'
                                            ' channel of the game to be tracked. '
                                            'Usage: {}track.'.format(bot.command_prefix)):
    logging.info("{}, code {} tried to track game: {}".format(ctx.message.author.display_name,
                                                              ctx.message.author, ctx.channel.name))
    game_name = ctx.message.channel.name
    db.update_tracked(dblogin, dbpw, game_name, 1)
    await ctx.channel.send('Game successfully tracked.')


@bot.command()
async def untrack(ctx, check=role_check, help='Untrack a game, must be used in game channel of the game to '
                                              'be untracked. Usage: {}untrack'.format(bot.command_prefix)):
    logging.info("{}, code {} tried to untrack game: {}".format(ctx.message.author.display_name, ctx.message.author,
                                                                ctx.channel.name))
    game_name = ctx.message.channel.name
    db.update_tracked(dblogin, dbpw, game_name, 0)
    await ctx.channel.send('Game successfully untracked.')


async def check_timers():
    await bot.wait_until_ready()
    while not bot.is_closed():
        for lists in db.get_tracked_port(dblogin, dbpw):
            logging.info(f'Checking timers for games: {lists}')
            for ports in lists:
                query_string = '{}/dom5.sh -T --tcpquery --ipadr 127.0.0.1 --port {}'.format(go.steamPath, ports)
                logging.info(f'port {ports} returned query string {query_string}.')
                game_status = await check_game_timer(query_string)
                if game_status is not None:
                    await game_status[1].send(game_status[0])
        await asyncio.sleep(1800)


async def after_colon(input_string):
    return input_string[input_string.find(':'):].strip()


async def check_game_timer(query_string):
    game_status = subprocess.check_output(shlex.split(query_string), universal_newlines=True)
    start_from = game_status.find('Gamename')
    trimmed_status = game_status[start_from:]
    trimmed_status_lines = trimmed_status.split('\n')
    after_colon_string = await after_colon(trimmed_status_lines[3])
    after_colon_string = after_colon_string.strip('ms :')
    time = int(after_colon_string)
    # less than an hour left
    if time <= 3600000:
        game_name = await after_colon(trimmed_status_lines[0])
        game_name = game_name.strip(' :') 
        turn_num = await after_colon(trimmed_status_lines[2])
        turn_num = turn_num.strip(' :')
        warning_message = '\n'
        channel_role = await get_role_by_name(bot.guilds[0], game_name)
        outstanding_turns = ['({}) With {}m remaining on turn {}, the following factions have outstanding turns:'
                                .format(channel_role.mention, time//60000, turn_num)]
        first_iteration = True
        for i in range(4, len(trimmed_status_lines)):
            player_line = await after_colon(trimmed_status_lines[i])
            player_line_status = player_line[player_line.find('(')+1:player_line.find(')')]
            if player_line_status in ['-', 'played, but not finished']:
                if first_iteration:
                    outstanding_turns.append('```')
                    first_iteration = False
                player_faction = player_line[player_line.find(':')+2:player_line.find('(')]
                outstanding_turns.append('\tUnfinished or unplayed turn from: {}'.format(player_faction))
        if not first_iteration:
            outstanding_turns.append('```')
        complete_message = warning_message.join(outstanding_turns)
        game_channel = await rac.get_channel_by_name(bot.guilds[0], game_name)
        return_list = [complete_message, game_channel]
        return return_list
    else:
        return None


@bot.command()
async def rank(ctx, rank: str, help='Attempts to toggle rank membership, usage: {}rank (rankname).'):
    logging.info("{}, code {} trying to give rank: {}"
                 .format(ctx.message.author.display_name, ctx.message.author, rank))
    requested_rank = await get_role_by_name(ctx.guild, rank)
    gm_rank = await get_role_by_name(ctx.guild, 'Gamemasters')
    if requested_rank is not None:
        if "_host" not in rank and requested_rank < gm_rank:
            has_role = await check_member_role(ctx.author, requested_rank)
            if has_role:
                # remove
                await rac.remove_role(ctx, requested_rank)
            else:
                # add
                await rac.add_role(ctx, requested_rank)
        else:
            ctx.channel.send('Requested rank can not be given by the bot.')
            logging.warning("".format())
    else:
        ctx.channel.send('Requested rank not found.')


@bot.command()
async def changemap(ctx, game_name:str, map_name: str):
    logging.info(f"{ctx.message.author.display_name}, code {ctx.message.author} attempting to change map of game "
                 f"{game_name} to {map_name}. ")
    try:
        db.change_map(dblogin, dbpw, game_name, map_name)
        await ctx.channel.send('Map successfully changed.')
    except (Exception, psycopg2.DatabaseError):
        logging.error(f"{ctx.message.author.display_name}, code {ctx.message.author} attempting to change map of "
                      f"game {game_name} to {map_name}. Failed. ")
        await ctx.channel.send('Map was not changed due to an error')
    

bot.loop.create_task(check_timers())
bot.run(bot_token)
