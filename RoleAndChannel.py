import discord
import logging


async def create_role_after_host(ctx, new_role_name):
    new_host_role = await ctx.guild.create_role(name=new_role_name, mentionable=True,
                                                permissions=discord.Permissions.none())
    return new_host_role


async def create_channel_after_host(ctx, new_channel_name):
    bot_category = await get_channel_category(ctx, 'bot-games')
    # new_channel = await ctx.guild.create_text_channel(new_channel_name)
    new_channel = await ctx.guild.create_text_channel(new_channel_name, category=bot_category)
    overwrite = discord.PermissionOverwrite()
    overwrite.manage_messages = True
    await new_channel.set_permissions(ctx.author, overwrite=overwrite)  
    return new_channel  


async def give_host_role(ctx, host_role):
    try:
        await ctx.author.add_roles(host_role, reason='Adding host role')
    except (discord.Forbidden, discord.HTTPException):
        await ctx.channel.send("Failed to give host role.")
        logging.error(f'{ctx.message.author.display_name}, code {ctx.message.author} was unable to receive the '
                      f'host role due to an error, name {host_role.name}')


async def get_channel_category(ctx, category_name):
    for category in ctx.guild.categories:
        if category.name == category_name:
            return category
    return None


async def get_channel_by_name(guild, channel_name):
    for channel in guild.channels:
        if channel.name == channel_name:
            return channel
    return None


async def create_game_role(ctx, game_name):
    new_game_role = await ctx.guild.create_role(name=game_name, mentionable=True,
                                                permissions=discord.Permissions.none())
    return new_game_role


async def remove_role(ctx, role):
    try:
        await ctx.author.remove_roles(role, reason='Request to bot.')
        await ctx.channel.send('{} Role removed successfully.'.format(ctx.author.mention))
    except (discord.Forbidden, discord.HTTPException) as e:
        await ctx.channel.send('{} Removing role failed.'.format(ctx.author.mention))
        logging.error(f'{ctx.author}, {ctx.author.mention} tried to remove a role and failed with exception {e}')


async def add_role(ctx, role):
    try:
        await ctx.author.add_roles(role, reason='Request to bot.')
        await ctx.channel.send('{} Role added successfully.'.format(ctx.author.mention))
    except (discord.Forbidden, discord.HTTPException) as e:
        await ctx.channel.send('{} Adding role failed.'.format(ctx.author.mention))
        logging.error(f'{ctx.author}, {ctx.author.mention} tried to add a role and failed with exception {e}')
