import asyncio
import os
import shlex
import subprocess
import Database as db
import discord
import logging

savesPath = os.path.abspath(os.environ['DOM5_SAVE'])
modsPath = os.path.abspath(os.environ['DOM5_MODS'])
mapsPath = os.path.abspath(os.environ['DOM5_LOCALMAPS'])
steamPath = os.path.abspath(os.environ['EXEDIR'])
backupScript = '/home/normal/scripts/turnBackupScript.sh'
gameCount = 0
gamePort = 1200
gameCap = 5


class GameOptions:
    # TODO check init from query, check insert_game and where it is called
    def __init__(self):
        self.game_name = ''
        self.map = ''
        self.mod_list = [None]
        self.age = -1
        self.research = -1
        self.research_spread = ''
        self.hof_size = -1
        self.independents_str = -1
        self.magic_sites = -1
        self.thrones = [-1, -1, -1, -1]
        self.cataclysm = -1
        self.event_rarity = -1
        self.events = ''
        self.globals = -1
        self.scoregraphs = ''
        self.disciples = ''
        self.master_pass = ''
        self.hosting_interval = -1

    def init_from_query(self, row):
        if len(row) != 21:
            logging.error(f'Tried to make game options from a malformed row.')
        else:
            self.game_name = row[0]
            self.map = row[1]
            if row[2] is 'none':
                self.mod_list = ''
            else:
                # TODO check how row represents multiple mods after insert
                self.mod_list = row[2].split(',')
            self.age = row[3]
            self.research = row[4]
            self.research_spread = row[5]
            self.hof_size = row[6]
            self.independents_str = row[7]
            self.magic_sites = row[8]
            self.thrones = row[9]
            print(self.thrones)
            self.cataclysm = row[10]
            self.event_rarity = row[11]
            self.events = row[12]
            self.globals = row[13]
            self.scoregraphs = row[14]
            self.disciples = row[15]
            self.master_pass = row[16]
            self.hosting_interval = row[17]

    def __repr__(self):
        return f'name: {self.game_name}, map: {self.map}, mods: {self.mod_list}, age: {self.age}, ' \
               f'research: {self.research}, spread: {self.research_spread}, hof: {self.hof_size}, ' \
               f'indeps: {self.independents_str}, sites: {self.magic_sites}, thrones: {self.thrones}, ' \
               f'cata: {self.cataclysm}, event_rar: {self.event_rarity}, events_str: {self.events}, ' \
               f'globals: {self.globals}, graphs: {self.scoregraphs}, disciples: {self.disciples}, ' \
               f'pass: {self.master_pass}, interval: {self.hosting_interval}'


async def request_game_options(call_context):
    msg_author = call_context.message.author
    game_opts = GameOptions()
    await msg_author.send('Welcome to the Dom5HostBot')
    try:
        game_opts.game_name = await request_game_name(call_context)
        game_opts.map = await request_map(call_context)
        game_opts.mod_list = await request_mods(call_context)
        game_opts.age = await request_age(call_context)
        game_opts.research = await request_research(call_context)
        game_opts.research_spread = await request_research_spread(call_context)
        game_opts.hof_size = await request_hof_size(call_context)
        game_opts.independents_str = await request_independents_str(call_context)
        game_opts.magic_sites = await request_magic_sites(call_context)
        game_opts.thrones = await request_thrones(call_context)
        game_opts.cataclysm = await request_cataclysm(call_context)
        game_opts.event_rarity = await request_event_rarity(call_context)
        game_opts.events = await request_events(call_context)
        game_opts.globals = await request_globals(call_context)
        game_opts.scoregraphs = await request_scoregraphs(call_context)
        game_opts.disciples = await request_disciples(call_context)
        game_opts.master_pass = await request_master_pass(call_context)
        game_opts.hosting_interval = await request_hosting_interval(call_context)
    except asyncio.TimeoutError:
        return None

    return game_opts


def find_open_ports(dblogin, dbpw):
    query_result = db.used_ports(dblogin, dbpw)
    usable_ports = set(range(gamePort, gamePort + gameCap))
    for rows in query_result:
        for port in rows:
            usable_ports.discard(int(port))
    return usable_ports


async def attempt_host(game_options, open_port):
    """Command list taken from: http://www.illwinter.com/dom5/techmanual.html"""
    game_commands_list = ['screen -S {0} -d -m {1}/dom5.sh -TS --noclientstart --renaming'.format(game_options.game_name,
                                                                                          steamPath),
                          ' --port ' + str(open_port), ' --mapfile ' + game_options.map, ' ' + game_options.game_name]
    # print(game_options.mod_list)
    for mod_name in game_options.mod_list:
        if mod_name != 'None':
            game_commands_list.append(' --enablemod ' + mod_name.strip())
 
    game_commands_list.append(' --era ' + str(game_options.age))
    game_commands_list.append(' --research ' + str(game_options.research))

    if game_options.research_spread == 't':
        game_commands_list.append(' --norandres')

    game_options.thrones = eval(game_options.thrones.replace('{', '[').replace('}', ']'))
    game_commands_list.append(f' --thrones {game_options.thrones[0]} '
                              f'{game_options.thrones[1]} {game_options.thrones[2]}')
    game_commands_list.append(f' --requiredap {game_options.thrones[3]}')

    game_commands_list.append(' --hofsize ' + str(game_options.hof_size))
    game_commands_list.append(' --indepstr ' + str(game_options.independents_str))
    game_commands_list.append(' --magicsites ' + str(game_options.magic_sites))

    if game_options.cataclysm != 0:
        game_commands_list.append(' --cataclysm ' + str(game_options.cataclysm))

    game_commands_list.append(' --eventrarity ' + str(game_options.event_rarity))

    if game_options.events == 'off':
        game_commands_list.append(' --nostoryevents')
    elif game_options.events == 'minor':
        game_commands_list.append(' --storyevents')
    elif game_options.events == 'full':
        game_commands_list.append(' --allstoryevents')

    game_commands_list.append(' --globals ' + str(game_options.globals))

    if game_options.scoregraphs == 'on':
        game_commands_list.append(' --scoregraphs')
    elif game_options.scoregraphs == 'disabled':
        game_commands_list.append(' --nonationinfo')

    if game_options.disciples == 'on':
        game_commands_list.append(' --teamgame')

    game_commands_list.append(' --masterpass ' + game_options.master_pass)
    game_commands_list.append(' --minutes ' + str(game_options.hosting_interval))
    game_commands_list.append(' --preexec {}/{}/turnBackupScript.sh'.format(savesPath, game_options.game_name))
    final_command_list = ''.join(game_commands_list)
    if gameCount < gameCap:
        p = subprocess.call(shlex.split(final_command_list))
        print(final_command_list)
        if p == 0:
            subprocess.call(shlex.split('mkdir -p {}/{}'.format(savesPath, game_options.game_name)))
            with open('{}/{}/startCommands.txt'.format(savesPath, game_options.game_name), 'a') as startCommands:
                startCommands.write(final_command_list)
                startCommands.close()
        return p
    else:
        return -1


async def request_game_name(ctx):
    msg_author = ctx.message.author
    await msg_author.send('What is the name of the game? This name should not contain spaces.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while os.path.isdir(os.path.join(savesPath, msg.content)):
        await msg_author.send('Game name already in use, or contains spaces please input another.')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content.lower()


async def request_map(ctx):
    msg_author = ctx.message.author
    map_dir = mapsPath
    command_character = ctx.bot.command_prefix
    await msg_author.send('What is the name of the map you would like to play? Type {}listmaps for a list of '
                          'available maps.'.format(command_character))

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while not os.path.isfile(map_dir  + '/' +  msg.content):
        if msg.content != '{}listmaps'.format(command_character):
            await msg_author.send('Map not found, try listing available maps with {}listmaps'.format(command_character))
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content


async def request_mods(ctx):
    mod_dir = modsPath + '/'
    msg_author = ctx.message.author
    command_character = ctx.bot.command_prefix
    await msg_author.send('What mods would you like enabled? Each seperate mod should be comma seperated, '
                          'type {}listmods for a list of available mods. Type \'None\' for no mods.'
                          .format(command_character))

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    if msg.content.lower() == 'none':
        return 'none'
    split_message = msg.content.replace(' ', '').split(',')
    verified = await verify_mods_exist(split_message, mod_dir)
    while not verified:
        await msg_author.send('One or more mods were not found, please re-enter the comma seperated list of mods, '
                              'or type {}listmods to list available mods'.format(command_character))
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        if msg.content.lower() == 'none':
            return 'none'
        split_message = msg.content.split(',')
        verified = await verify_mods_exist(split_message, mod_dir)
    return msg.content


async def request_age(ctx):
    msg_author = ctx.message.author
    await msg_author.send('What age? 1 for EA, 2 for MA and 3 for LA.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['1', '2', '3']:
        await msg_author.send('Incorrect age entered, please select one 1 for EA, 2 for MA or 3 for LA.')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return int(msg.content)


async def request_research(ctx):
    msg_author = ctx.message.author
    await msg_author.send('What is the required research difficulty? Select either 0 (very easy), 1 (easy), '
                          '2 (normal), 3 (hard) or 4 (very hard)')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['0', '1', '2', '3', '4']:
        await msg_author.send('Incorrect research difficulty entered, please select either 0 (very easy), 1 (easy), '
                              '2 (normal), 3 (hard) or 4 (very hard).')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return int(msg.content)


async def request_research_spread(ctx):
    msg_author = ctx.message.author
    await msg_author.send('What is the required research spread: random or spread?')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['random', 'spread']:
        await msg_author.send('Incorrect research spread entered, please choose either \'random\' or \'spread\'')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    if msg.content == 'spread':
        return 't'
    else:
        return 'f'


async def request_hof_size(ctx):
    msg_author = ctx.message.author

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    await msg_author.send('Hall of fame size?')
    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            int(msg.content)
            return int(msg.content)
        except ValueError:
            await msg_author.send('Hall of fame size must be entered as a number.')
            pass


async def request_independents_str(ctx):
    msg_author = ctx.message.author

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    await msg_author.send('Indepedents strength? (0-9 inclusive, 5 default)')
    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            if int(msg.content) not in range(10):
                await msg_author.send('Independent strength must be between 0 and 9 inclusive.')
            else:
                return int(msg.content)
        except ValueError:
            await msg_author.send('Independent strength must be a number')
            pass


async def request_magic_sites(ctx):
    msg_author = ctx.message.author

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    await msg_author.send('Magic site frequency? (0 - 75 inclusive, 60 is the default for EA, 50 MA and 40 LA.)')
    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            if int(msg.content) not in range(76):
                await msg_author.send('Magic site frequency must be between 0 and 75 inclusive.')
            else:
                return int(msg.content)
        except ValueError:
            await msg_author.send('Magic site frequency must be a number')
            pass


async def request_thrones(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please enter the amount of level 1, 2 and 3 thrones and the amount of ascension points '
                          'to win seperated by commas. (e.g: \'1, 2, 3, 4\').')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    thrones_validated = await interpret_throne_settings(msg.content)
    while thrones_validated == [None]:
        await msg_author.send('Throne settings formatted incorrectly. Please enter the amount of level 1, 2 and 3 '
                              'thrones and the amount of ascension points to win seperated by commas. '
                              '(e.g: \'1, 2, 3, 4\').')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        thrones_validated = await interpret_throne_settings(msg.content)
    return thrones_validated


async def verify_mods_exist(mod_name_list, mod_directory):
    for mods in mod_name_list:
        if not os.path.isfile(mod_directory + mods.strip()):
            return False
    return True


async def request_cataclysm(ctx):
    msg_author = ctx.message.author

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    await msg_author.send('Please input the number of turns until cataclysm activates, input 0 to disable it.')
    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            int(msg.content)
            return int(msg.content)
        except ValueError:
            await msg_author.send('Turns until cataclysm must be a number.')
            pass


async def request_event_rarity(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input the event rarity, 1 is common, 2 is rare.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            if int(msg.content) not in [1, 2]:
                await msg_author.send('Event rarity must be either 1 (common) or 2 (rare).')
            else:
                return int(msg.content)
        except ValueError:
            await msg_author.send('Event rarity must be a number.')
            pass


async def request_events(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input the level of story events: \'minor\', \'full\' or \'off\','
                          ' the default is minor.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['minor', 'full', 'off']:
        await msg_author.send('Event level must be either \'minor\', \'full\' or \'off\'.')   
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content


async def request_globals(ctx):
    msg_author = ctx.message.author

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    await msg_author.send('Please input the maximum number of globals.')
    while True:
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        try:
            int(msg.content)
            return int(msg.content)
        except ValueError:
            await msg_author.send('The maximum number of globals must be a number.')
            pass


async def request_scoregraphs(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input the availability of score graphs, \'on\', \'off\' or \'disabled\', '
                          'the default is off, disabled turns off any information even through sites.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['on', 'off', 'disabled']:
        await msg_author.send('Scoregraphs must be either on, off or disabled.')   
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content


async def request_disciples(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input whether this game is a disciples game by either saying \'on\' or \'off\'.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    while msg.content not in ['on', 'off']:
        await msg_author.send('The game must either be a disciples game (\'on\') or not (\'off\').')   
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content


async def request_master_pass(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input the master password, must be a single word, otherwise it will default'
                          ' to the first word.')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)
    
    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    return msg.content


async def request_hosting_interval(ctx):
    msg_author = ctx.message.author
    await msg_author.send('Please input the hosting interval in the format XdYhZm, e.g. 0d22h15m')

    def pm_check(message):
        return msg_author == message.author and isinstance(message.channel, discord.DMChannel)

    msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
    interval_in_minutes = await interpret_host_interval(msg.content)
    while interval_in_minutes == -1:
        await msg_author.send('Error, please input the hosting interval in the format XdYhZm, e.g. 0d22h15m')
        msg = await ctx.bot.wait_for('message', check=pm_check, timeout=60.0)
        interval_in_minutes = await interpret_host_interval(msg.content)
    return interval_in_minutes


async def interpret_throne_settings(throne_string):
    number_list = throne_string.split(',')
    if len(number_list) != 4:
        return [None]
    try:
        number_list[0] = int(number_list[0])
    except ValueError:
        return [None]
    try:
        number_list[1] = int(number_list[1])
    except ValueError:
        return [None]
    try:
        number_list[2] = int(number_list[2])
    except ValueError:
        return [None]
    try:
        number_list[3] = int(number_list[3])
    except ValueError:
        return [None]
    return number_list


async def interpret_host_interval(interval_string):
    days_position = interval_string.find('d')
    hours_position = interval_string.find('h')
    minutes_position = interval_string.find('m')
    host_interval_in_minutes = 0
    if days_position == -1 or hours_position == -1 or minutes_position == -1:
        return -1
    else:
        try:
            host_interval_in_minutes += int(interval_string[:days_position]) * 1440
        except ValueError:
            return -1
        try:
            host_interval_in_minutes += int(interval_string[days_position+1:hours_position]) * 60
        except ValueError:
            return -1
        try:
            host_interval_in_minutes += int(interval_string[hours_position+1:minutes_position])
        except ValueError:
            return -1
        return host_interval_in_minutes


async def find_running_games():
    games_dir = savesPath
    games_count = 0
    for f in os.listdir(games_dir):
        child = os.path.join(games_dir, f)
        if os.path.isdir(child):
            games_count += 1
    return games_count


async def hosted_game_string(game_options, game_port):
    return_string = '```Game name: {}\nMap name: {}\nMod list: {}\nAge: {}\nResearch: {}\n\
Research spread: {}\nHoF size: {}\nIndependents strength: {}\nMagic site frequency: {}\n\
Throne settings: {}\nCataclysm timer: {}\nEvent rarity: {}\nEvent magnitude: {}\nMax globals: {}\n\
Scoregraphs: {}\nDisciples: {}\nMaster pass: {}\nHosting interval: {}\nIP: {}\nPort: {}```\n\
Remember to remove the master password!'.format(game_options.game_name, game_options.map, game_options.mod_list,
                                                game_options.age, game_options.research, game_options.research_spread,
                                                game_options.hof_size, game_options.independents_str,
                                                game_options.magic_sites, game_options.thrones, game_options.cataclysm,
                                                game_options.event_rarity, game_options.events, game_options.globals,
                                                game_options.scoregraphs, game_options.disciples,
                                                game_options.master_pass, '{}h{}m.'
                                                .format(game_options.hosting_interval // 60,
                                                        game_options.hosting_interval % 60),
                                                'cssc.asn.au', game_port)
    return return_string


async def untrack_port(ctx, tracked_games):
    pinned_messages = await ctx.channel.pins()
    creation_pin = pinned_messages[len(pinned_messages)-1]
    game_port = creation_pin.content.splitlines()[-2].split(' ')[1].strip('`')
    tracked_path = '{}/trackedgames.txt'.format(savesPath)
    if game_port in tracked_games:
        with open(tracked_path, 'r') as tracked_file:
            keep_games = []
            for line in tracked_file:
                if game_port != line.strip():
                    keep_games.append(line)
            tracked_file.close()
        with open(tracked_path, 'w') as tracked_file:
            for tracked_game in keep_games:
                tracked_file.write('{}\n'.format(tracked_game))
            tracked_file.close()
        tracked_games.remove(game_port)
        await ctx.message.channel.send('Game removed from tracking list')
        return tracked_games
    else:
        await ctx.channel.send('Game is currently not being tracked.')

