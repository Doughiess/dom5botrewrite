import psycopg2
import GameOptions as go
import random
import logging

def get_all_games(dblogin, dbpw):
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')
    data = []
    try:
        query = 'select * from gameslist;'
        with conn.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()
        return data


def used_ports(dblogin, dbpw):
    query = 'select port from gameslist;'
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')
    data = []
    try:
        with conn.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()
        return data


def game_port_from_name(dblogin, dbpw, game_name):
    query = 'select port from gameslist where gamename;'
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')
    data = []
    try:
        with conn.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()
        return data


def get_tracked_port(dblogin, dbpw):
    query = 'select port from gameslist where tracked = TRUE;'
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')
    data = []
    try:
        with conn.cursor() as cursor:
            cursor.execute(query)
            data = cursor.fetchall()
    except (Exception, psycopg2.DatabaseError):
            raise
    finally:
        conn.close()
        return data


def insert_game(dblogin, dbpw, fields):
    query = "INSERT INTO gameslist (name, mapname, modlist, age, researchdiff, randresearch, hofsize, indepstr, " \
            "siterarity, thrones, cataclysm, eventrarity, storyevents, globals, graphs, disciples, masterpass, " \
            "interval, port, postscripts, tracked) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s," \
            "%s, %s, %s, %s, %s, %s, %s);"
    port_set = go.find_open_ports(dblogin, dbpw)
    port = random.choice(tuple(port_set))
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')

    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (fields.game_name, fields.map, fields.mod_list, fields.age, fields.research,
                                   fields.research_spread, fields.hof_size, fields.independents_str, fields.magic_sites,
                                   fields.thrones, fields.cataclysm, fields.event_rarity, fields.events, fields.globals,
                                   fields.scoregraphs, fields.disciples, fields.master_pass, fields.hosting_interval,
                                   port, go.backupScript, '0'))
            conn.commit()
    except (Exception, psycopg2.DatabaseError):
        conn.rollback()
        raise
    finally:
        conn.close()
    return port


def remove_game(dblogin, dbpw, game_name):
    query = "delete from gameslist where name=%s;"
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')

    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (game_name,))
            conn.commit()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()


def update_tracked(dblogin, dbpw, game_name, track):
    query = "update gameslist set tracked=%s where name=%s;"
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')

    try:
        with conn.cursor() as cursor:
            if track == 1:
                cursor.execute(query, ("TRUE", game_name))
            if track == 0:
                cursor.execute(query, ("FALSE", game_name))
            conn.commit()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()


def change_map(dblogin, dbpw, game_name, map_name):
    query = "update gameslist set mapname=%s where name=%s;"
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')

    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (map_name, game_name))
            conn.commit()
    except (Exception, psycopg2.DatabaseError):
        conn.rollback()
        raise
    finally:
        conn.close()


def change_timer(dblogin, dbpw, game_name, time_in_hours):
    query = "update gameslist set interval=%s where name=%s;"
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')

    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (time_in_hours, game_name))
            conn.commit()
    except (Exception, psycopg2.DatabaseError) as e:
        conn.rollback()
        logging.error(f'DB updated error on change timer: {e}')
        raise
    finally:
        conn.close()


def get_game_details(dblogin, dbpw, game_name):
    query = "select * from gameslist where name=%s;"
    conn = psycopg2.connect(host='localhost', user=dblogin, password=dbpw, dbname='games')
    data = []
    try:
        with conn.cursor() as cursor:
            cursor.execute(query, (game_name,))
            data = cursor.fetchall()
    except (Exception, psycopg2.DatabaseError):
        raise
    finally:
        conn.close()
        return data
