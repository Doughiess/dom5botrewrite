# Description
Discord bot to automate hosting of Dominions 5 games, written using the Discord.py API wrapper ( https://github.com/Rapptz/discord.py ). Requires but doesn't set up a postgresql database, sorry!

# Setup
1. Clone
2. Setup postgresql database that matches the details found in Database.py
3. Run python dom5hostbot.py and input bot token